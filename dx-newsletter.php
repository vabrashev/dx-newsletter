<?php
/**
 * Plugin name: DX Newsletter
 * Description: DX Newsletter
 * Author: vabrashev
 * Author URI: https://me.bg
 * Version: 1.0.0
 **/

namespace DX\NL;

defined( 'ABSPATH' ) or die( 'You cannot access this page directly.' );

if ( ! defined( 'DXNL_DIR' ) ) {
	define( 'DXNL_DIR', dirname( __FILE__ ) . '/' );
}

if ( ! defined( 'DXNL_URL' ) ) {
	define( 'DXNL_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'DXNL_ASSET_VERSION' ) ) {
	define( 'DXNL_ASSET_VERSION', '202115010000' );
}

/**
 * Class Schema_Yoast_Extension
 *
 * This is the bootloader class that load and initialize the plugin's classes/traits.
 *
 * @package DX\NL
 */
class DX_Newsletter {

	public function __construct() {
		add_action( 'plugins_loaded', array( $this, 'include_classes' ), 10 );
		add_action( 'init', array( $this, 'init' ), 20 );
	}

	/**
	 * This function includes all the classes/traits on the plugins_loaded with priority
	 * 10 in order to be easily overwritten.
	 */
	public function include_classes() {
		require DXNL_DIR . 'includes/class-nl-main.php';
		require DXNL_DIR . 'includes/class-custom-post-types.php';
		require DXNL_DIR . 'includes/class-custom-post-statuses.php';
		require DXNL_DIR . 'includes/class-metaboxes.php';
		require DXNL_DIR . 'includes/class-ajax.php';
		require DXNL_DIR . 'includes/class-custom-api.php';
	}

	/**
	 * This class initializes the classes/traits on initialization of WordPress.
	 */
	public function init() {
		new NL_Main_Class();
		new Custom_Post_Types();
		new Custom_Post_Statuses();
		new Metaboxes();
		new Ajax();
	}

}

new DX_Newsletter();
