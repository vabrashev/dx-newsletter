<?php

namespace DX\NL;

defined( 'ABSPATH' ) or die( 'You cannot access this page directly.' );

/**
 * Class Ajax
 *
 * This class is used to generate responses to the requests that are send to the plugin
 *
 * @package DX\NL
 */
class Ajax {
	/**
	 * @var $no_results_string The string when the query is empty.
	 */
	private $no_results_string = 'No results found';
	/**
	 * @var $min_results_letters Minimum characters for search to be triggered.
	 */
	private $min_results_letters = 5;
	/**
	 * @var $def_posts_per_page Default posts number on initialization and after search.
	 */
	private $def_posts_per_page = 30;

	/**
	 * Ajax constructor.
	 */
	public function __construct() {
		add_action( 'wp_ajax_dx_rss_get_posts', array( $this, 'dx_get_posts' ) );
		add_action( 'wp_ajax_dx_rss_save_post', array( $this, 'dx_save_post' ) );
	}

	/**
	 * Function that is called by the JS via AJAX, hooked to the dx_rss_get_posts action.
	 * It returns all matched posts
	 */
	public function dx_get_posts() {
		$posts_string = '';

		$_POST['post_id'] = esc_attr( $_POST['post_id'] );
		$_POST['keyword'] = esc_attr( $_POST['keyword'] );

		if ( ! empty( $_POST['selected_posts'] ) ) {
			$arr_selected_posts = array_flip( $_POST['selected_posts'] );
		}

		if ( empty( $_POST['keyword'] ) ) {
			$the_query = new \WP_Query(
				array(
					'posts_per_page' => $this->def_posts_per_page,
					'post_type'      => 'post',
					'post_status'    => 'publish',
				)
			);

			if ( $the_query->have_posts() ) {
				while ( $the_query->have_posts() ) {
					$the_query->the_post();

					$disable = '';
					if ( ! empty( $arr_selected_posts ) && array_key_exists( get_the_ID(), $arr_selected_posts ) ) {
						$disable = ' disabled';
					}

					$posts_string .= '<li><span class="dx-rss-rel-item' . $disable . '" data-id="' . get_the_ID() . '">' . get_the_title() . '</span></li>';

				}
				wp_reset_postdata();
			}
		} elseif ( strlen( $_POST['keyword'] ) > $this->min_results_letters ) {
			$the_query = new \WP_Query(
				array(
					'posts_per_page' => $this->def_posts_per_page,
					's'              => $_POST['keyword'],
					'post_type'      => 'post',
				)
			);

			if ( $the_query->have_posts() ) {
				while ( $the_query->have_posts() ) {
					$the_query->the_post();

					$disable = '';
					if ( ! empty( $arr_selected_posts ) && array_key_exists( get_the_ID(), $arr_selected_posts ) ) {
						$disable = ' disabled';
					}

					$posts_string .= '<li><span class="dx-rss-rel-item' . $disable . '" data-id="' . get_the_ID() . '">' . get_the_title() . '</span></li>';

				}
				wp_reset_postdata();
			} else {
				$posts_string = '<li><span class="dx-rss-rel-item disabled">' . $this->no_results_string . '</span></li>';
			}
		}

		if ( ! empty( $posts_string ) ) {
			wp_send_json_success( $posts_string );
		}
	}

	/**
	 * Function that is called by the JS via AJAX, hooked to the dx_rss_save_post action.
	 * It checks if we have more than 1 published campaign
	 */
	public function dx_save_post() {
		check_ajax_referer( 'dx-ajax-nonce', 'security' );

		$post_id     = esc_attr( $_POST['post_id'] );
		$post_status = get_post_status($post_id);
		//$publish_checkbox = esc_attr( $_POST['dx_rss_publish_checkbox'] );

		$count_posts = wp_count_posts( 'dx_rss_feed' )->publish;

		if ( $count_posts > 0 && 'publish' != $post_status ) {
			echo 'You cannot have more than 1 published campaign!';
		} else {
			echo 'true';
		}

		die();
	}

}
