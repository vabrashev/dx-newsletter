<?php

namespace DX\NL;

defined( 'ABSPATH' ) or die( 'You cannot access this page directly.' );

/**
 * Class RSS_API
 * This is the main class.
 */
class RSS_API {
	/**
	 * RSS_API class constructor.
	 */
	public function __construct() {
		add_action( 'rest_api_init', array( $this, 'register_dx_api' ) );
	}

	/**
	 * Register WP REST URL.
	 */
	public function register_dx_api() {
		register_rest_route(
			'dx-mc-api/v1',
			'/dx-mc-api',
			array(
				'methods'  => 'POST',
				'callback' => array( $this, 'dx_mc_api' ),
			)
		);
	}

	/**
	 * Main function for MC Webhook requests.
	 */
	public function dx_mc_api( $request ) {
		if ( empty( $_POST['data'] ) && 'sent' != $_POST['data']['sent'] ) {
			return;
		}

		$post_array = filter_input_array( INPUT_POST, FILTER_SANITIZE_STRING );

		$rss_feeds = new \WP_Query(
			array(
				'posts_per_page' => 1,
				'post_type'      => 'dx_rss_feed',
				'post_status'    => 'publish',
			)
		);

		if ( $rss_feeds->have_posts() ) {
			while ( $rss_feeds->have_posts() ) {
				$rss_feeds->the_post();
				$post_id = get_the_ID();

				wp_update_post(
					array(
						'ID'          => $post_id,
						'post_status' => 'dx_rss_sent',
					)
				);

				update_post_meta( $post_id, '_dx_rss_sent_info', $post_array );
			}
			wp_reset_postdata();
		}
	}
}
