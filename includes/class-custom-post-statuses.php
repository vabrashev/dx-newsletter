<?php

namespace DX\NL;

defined( 'ABSPATH' ) or die( 'You cannot access this page directly.' );

/**
 * Class Custom_Post_Statuses
 *
 * This class creates custom post statuses
 *
 * @package DX\NL
 */
class Custom_Post_Statuses {

	/**
	 * Products constructor.
	 */
	public function __construct() {
		$this->register_custom_post_status();
	}

	/**
	 * Function that registers the custom post status dx_rss_feed.
	 */
	public function register_custom_post_status() {

		$args = array(
			'label'                     => _x( 'Sent', 'post' ),
			'public'                    => true,
			'exclude_from_search'       => false,
			'post_type'                 => array( 'dx_rss_feed' ),
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => true,
			'label_count'               => _n_noop( 'Sent <span class="count">(%s)</span>', 'Sent <span class="count">(%s)</span>' ),
		);

		/**
		 * Add 'Unread' post status.
		 */
		register_post_status( 'dx_rss_sent', $args );
	}
}
