<?php

namespace DX\NL;

defined( 'ABSPATH' ) or die( 'You cannot access this page directly.' );

/**
 * Class Custom_Post_Types
 *
 * This class creates custom post types
 *
 * @package DX\NL
 */
class Custom_Post_Types {

	/**
	 * Products constructor.
	 */
	public function __construct() {
		$this->register_custom_post_type();
	}

	/**
	 * Function that registers the custom post type dx_rss_feed.
	 */
	public function register_custom_post_type() {

		$labels = array(
			'name'               => 'DX Newsletter',
			'singular_name'      => 'DX Newsletter Feed',
			'add_new'            => 'Add New',
			'all_items'          => 'All Templates',
			'add_new_item'       => 'Add New',
			'edit_item'          => 'Edit Template',
			'new_item'           => 'New Template',
			'view_item'          => 'View Template',
			'search_item'        => 'Search Templates',
			'not_found'          => 'No feed found',
			'not_found_in_trash' => 'No feed found in trash',
			'parent_item_colon'  => 'Parent Templates',
		);
		$args   = array(
			'labels'          => $labels,
			'public'          => false,
			'show_ui'         => true,
			'query_var'       => true,
			'rewrite'         => true,
			'capability_type' => 'post',
			'hierarchical'    => false,
			'supports'        => array(
				'title',
				'thumbnail',
			),
			'menu_position'   => 20,
		);

		register_post_type( 'dx_newsletter', $args );
	}
}
