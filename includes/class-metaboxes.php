<?php

namespace DX\NL;

defined( 'ABSPATH' ) or die( 'You cannot access this page directly.' );

/**
 * Class Metaboxes
 *
 * This class creates custom meta boxes and generates their views
 *
 * @package DX\NL
 */
class Metaboxes {

	/**
	 * Metaboxes constructor.
	 */
	public function __construct() {
		add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
	}

	/**
	 * Function that adds a meta box for displaying the attributes in the dx_rss_feed_posts custom post type.
	 */
	public function add_meta_box() {

		add_meta_box(
			'dx_rss_feed_posts',
			'RSS Feed Posts',
			array( $this, 'dx_rss_posts_html' ),
			'dx_rss_feed',
			'normal',
			'high'
		);

		add_meta_box(
			'dx_rss_custom_content',
			'Content',
			array( $this, 'dx_rss_custom_content_html' ),
			'dx_rss_feed',
			'normal',
			'high'
		);

		add_meta_box(
			'dx_rss_feed_confirm_cb',
			'Publish Confirmation',
			array( $this, 'dx_rss_confirm_cb_html' ),
			'dx_rss_feed',
			'side',
			'high'
		);

		add_meta_box(
			'dx_rss_feed_sent_info',
			'Campaign Sent Info',
			array( $this, 'dx_rss_feed_sent_info_html' ),
			'dx_rss_feed',
			'side',
			'high'
		);
	}

	/**
	 * Function that is passed to the add_meta_box() for printing the markup for the meta box.
	 *
	 * @param $post - Receives the current post.
	 */
	public function dx_rss_posts_html( $post ) {
		$get_selected_posts = get_post_meta( $post->ID, '_dx_rss_selected_posts', true );

		$the_query = new \WP_Query(
			array(
				'posts_per_page' => 30,
				'post_type'      => 'post',
				'post_status'    => 'publish',
			)
		);

		// Validation for the output data
		if ( ! empty( $get_selected_posts ) ) {

			$the_query_selected = new \WP_Query(
				array(
					'post_type'   => 'post',
					'post_status' => 'publish',
					'post__in'    => $get_selected_posts,
					'orderby'     => 'post__in',
				)
			);

			if ( $the_query_selected->have_posts() ) {
				while ( $the_query_selected->have_posts() ) {
					$the_query_selected->the_post();

					$arr_selected_posts[ get_the_ID() ] = get_the_title();

				}
				wp_reset_postdata();
			}
		}

		// Including the view where the data above is used
		require DXNL_DIR . 'views/dx-rss-selected-posts-view.php';
	}

	/**
	 * Function that is passed to the add_meta_box() for printing the markup for the meta box.
	 *
	 * @param $post - Receives the current post.
	 */
	public function dx_rss_custom_content_html( $post ) {
		$content = get_post_meta( $post->ID, '_dx_rss_custom_content', true );

		if ( empty( $content ) ) {
			$content = '';
		}

		wp_editor(
			$content,
			'_dx_rss_custom_content',
			array( 'media_buttons' => true )
		);
	}

	/**
	 * Function that is passed to the add_meta_box() for printing the markup for the meta box.
	 *
	 * @param $post - Receives the current post.
	 */
	public function dx_rss_confirm_cb_html( $post ) {
		echo '<input type="checkbox" name="_dx_rss_publish" id="_dx_rss_publish" value="true" />';
		echo '<label>Confirm if you want to publish</label>';
	}

	/**
	 * Function that is passed to the add_meta_box() for printing the markup for the meta box.
	 *
	 * @param $post - Receives the current post.
	 */
	public function dx_rss_feed_sent_info_html( $post ) {
		$campaign_sent_info = get_post_meta( $post->ID, '_dx_rss_sent_info', true );

		if ( empty( $campaign_sent_info ) ) {
			echo '<div>No sent info</div>';
			return;
		}

		$fired_at = $campaign_sent_info['fired_at'];
		$subject  = $campaign_sent_info['data']['subject'];

		echo '<label><strong>Last sent on:</strong></label>';
		echo '<div>' . $fired_at . '</div>';
		echo '<label><strong>Subject:</strong></label>';
		echo '<div>' . $subject . '</div>';
	}

}
