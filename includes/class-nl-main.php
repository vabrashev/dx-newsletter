<?php

namespace DX\NL;

defined( 'ABSPATH' ) or die( 'You cannot access this page directly.' );

/**
 * Class NL_Main_Class
 *
 * This is the main controller class of the plugin, here are done most of the operations.
 *
 * @package DX\NL
 */
class NL_Main_Class {

	// use Helpers;

	/**
	 * NL_Main_Class constructor.
	 */
	public function __construct() {

		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'save_post_dx_rss_feed', array( $this, 'save_post_meta' ) );
		add_action( 'untrashed_post', array( $this, 'dx_restore_post' ) );

		//add_action( 'manage_dx_rss_feed_posts_custom_column', array( $this, 'adding_custom_column' ), 10, 2 );
		//add_filter( 'manage_edit-dx_rss_feed_columns', array( $this, 'custom_columns' ) );

		add_feed( 'dx-rss-feed', array( $this, 'custom_rss_feed_content' ) );
		add_action( 'admin_menu', array( $this, 'dx_add_submenu_pages' ) );

		// Using this for non-hierarchical post types to hide Quick Edit.
		add_action( 'post_row_actions', array( $this, 'dx_hide_quick_edit' ), 10, 2 );
	}

	/**
	 * Function that adds submenu pages.
	 */
	public function dx_add_submenu_pages() {
		add_submenu_page( 'edit.php?post_type=dx_newsletter', 'Templates', 'Templates', 'manage_options', 'Templates-page', array( $this, 'settings_page_html' ), 10 );
		add_submenu_page( 'edit.php?post_type=dx_newsletter', 'Groups', 'Groups', 'manage_options', 'groups-page', array( $this, 'settings_page_html' ), 10 );
		add_submenu_page( 'edit.php?post_type=dx_newsletter', 'Emails', 'Emails', 'manage_options', 'emails-page', array( $this, 'settings_page_html' ), 10 );
		add_submenu_page( 'edit.php?post_type=dx_newsletter', 'Email log', 'Email log', 'manage_options', 'email-log-page', array( $this, 'settings_page_html' ), 10 );
		add_submenu_page( 'edit.php?post_type=dx_newsletter', 'Import/export emails', 'Import/export emails', 'manage_options', 'import-export-page', array( $this, 'settings_page_html' ), 10 );
	}

	/**
	* Function that changes the post status when restoring it from trash.
	*/
	public function dx_restore_post( $post_id ) {
		wp_update_post(
			array(
				'ID'          => $post_id,
				'post_status' => 'draft',
			)
		);
		return;
	}

	/**
	 * Function that hides Quick Edit option.
	 */
	function dx_hide_quick_edit( $actions, $post ) {
		if ( 'dx_rss_feed' == $post->post_type ) {
			unset( $actions['inline hide-if-no-js'] );
		}
		return $actions;
	}

	/**
	 * Function that shows the Settings page.
	 */
	public function settings_page_html() {
		//require_once DXNL_DIR . 'views/dx-nl-settings-page-view.php';
	}

	/**
	 * Function that saves the data that's coming from our custom post types meta box.
	 *
	 * @param $post_id - Receives the current's post id.
	 */
	public function save_post_meta( $post_id ) {

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}
		if ( false !== wp_is_post_revision( $post_id ) ) {
			return;
		}

		$key   = '_dx_rss_selected_posts';
		$input = array();

		if ( isset( $_POST[ $key ] ) ) {
			$input = $_POST[ $key ];
			$input = array_map( 'sanitize_text_field', $input );
		}

		update_post_meta( $post_id, $key, $input );

		$key   = '_dx_rss_custom_content';
		$input = array();

		if ( isset( $_POST[ $key ] ) ) {
			$input = $_POST[ $key ];
		}

		update_post_meta( $post_id, $key, $input );
	}

	/**
	 * Function that enqueues the scripts and styles only on the custom post type dx_rss_feed.
	 */
	public function enqueue_scripts( $page ) {

		if ( 'dx_rss_feed' === get_post_type() && ( 'post.php' == $page || 'post-new.php' == $page ) ) {
			wp_register_script( 'dx-rss-js', DXNL_URL . 'assets/scripts/scripts.js', array( 'jquery', 'jquery-ui-sortable' ), DXNL_ASSET_VERSION, true );
			wp_localize_script(
				'dx-rss-js',
				'dx_rss_params',
				array(
					'post_id'     => get_the_ID(),
					'post_status' => get_post_status(),
					'nonce'       => wp_create_nonce( 'dx-ajax-nonce' ),
					'ajaxurl'     => admin_url( 'admin-ajax.php' ),
				)
			);
			wp_enqueue_script( 'dx-rss-js' );

			wp_enqueue_style( 'dx-rss-css', DXNL_URL . 'assets/styles/styles.css', array(), DXNL_ASSET_VERSION );
		}
	}

	/**
	 * Function that shows the custom RSS feed.
	 */
	public function custom_rss_feed_content() {
		$rss_feeds = new \WP_Query(
			array(
				'posts_per_page' => 1,
				'post_type'      => 'dx_rss_feed',
				'post_status'    => 'publish',
			)
		);

		if ( $rss_feeds->have_posts() ) {
			while ( $rss_feeds->have_posts() ) {
				$rss_feeds->the_post();

				$post_title = get_the_title();

				$get_selected_posts = get_post_meta( get_the_ID(), '_dx_rss_selected_posts', true );

				$custom_content = get_post_meta( get_the_ID(), '_dx_rss_custom_content', true );
				$custom_content = wpautop( $custom_content );

				if ( ! empty( $get_selected_posts ) ) {
					$the_query_selected = new \WP_Query(
						array(
							'post_type'    => 'post',
							'post_status'  => 'publish',
							'post__in'     => $get_selected_posts,
							'orderby'      => 'post__in',
							'post__not_in' => get_option( 'sticky_posts' ),
						)
					);

					if ( $the_query_selected->have_posts() ) {
						while ( $the_query_selected->have_posts() ) {
							$the_query_selected->the_post();

							$arr_selected_posts[] = array(
								'post_title_rss'     => get_the_title_rss(),
								'post_permalink_rss' => esc_url( apply_filters( 'the_permalink_rss', get_permalink() ) ),
								'post_time'          => get_post_time( 'Y-m-d H:i:s', true ),
								'post_author'        => get_the_author(),
								'post_guid'          => get_the_guid(),
								'post_excerpt_rss'   => apply_filters( 'the_excerpt_rss', get_the_excerpt() ),
							);
						}
						wp_reset_postdata();
					}
				}
			}
			wp_reset_postdata();
		}

		$blog_title = get_bloginfo_rss( 'name' ) . ' Newsletter';
		if ( ! empty( $post_title ) ) {
			$blog_title = $post_title;
		}

		// Generate newsletter html for single article content.
		ob_start();
		include DXNL_DIR . 'includes/rss/dx-rss-mc-single.php';
		$content = ob_get_clean();

		if ( ! empty( $arr_selected_posts ) ) {
			$arr_selected_posts = array( array(
				'post_title_rss'     => '',
				'post_permalink_rss' => '',
				'post_time'          => date('D, d M Y H:i:s +0000'),
				'post_author'        => '',
				'post_guid'          => '',
				'post_excerpt_rss'   => $content,
			) );
		}

		require_once DXNL_DIR . 'includes/rss/dx-rss-mc.php';
	}

	/**
	 * Function that populating the custom columns.
	 */
	public function adding_custom_column( $name, $post_id ) {
		if ( 'dx-rss-selected-posts' == $name ) {
			// $type_id    = get_post_meta( $post_id, '_dx_rss_feed_type_id', true );
			// $attributes = get_post_meta( $post_id, '_dxpls_selected_permanent_attributes', true );

			if ( ! empty( $term = get_term( $type_id ) ) ) {
				echo $term->name;
			} else {
				echo '<span class="dashicons dashicons-minus"></span>';
			}
		}
	}

	/**
	 * Function that is adding a custom columns.
	 */
	public function custom_columns( $columns ) {
		return array(
			'cb'                    => '<input type="checkbox" />',
			'title'                 => 'Title',
			'dx-rss-selected-posts' => 'Selected Posts',
			'date'                  => 'Date',
		);
	}
}
